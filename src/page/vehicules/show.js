import React, { useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, CardTitle } from "reactstrap";
import { Link } from "react-router-dom";
import NavBar from '../navbar/NavBar';
import { getCar } from "../../redux/actions/vehicule-actions";
import { useDispatch, useSelector } from 'react-redux';

const Vehicule = ({ location }) => {
    const vehicule = useSelector(state => state.vehicule.vehicule);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCar(location.state.vehicule));
    }, [dispatch])
    
        return <div className={"container mt-5"}>
            <NavBar />
            <Card>
                <CardHeader>{vehicule && vehicule.modele && vehicule.modele.marque+" "+vehicule.modele.modele}</CardHeader>
                <CardBody>
                    <img className={"float-left"} width="308" height="183px" src={vehicule && vehicule.image && "http://127.0.0.1:8000/media/"+vehicule.image.filePath} alt="Card image cap" />
                    <CardTitle>Num Serie: {vehicule && vehicule.numSerie} </CardTitle>
                    <CardTitle>Matricule: {vehicule && vehicule.matricule} </CardTitle>
                    <CardTitle>Kilometrage: {vehicule && vehicule.kilometrage} km </CardTitle>
                    <CardTitle>Couleur: {vehicule && vehicule.couleur} </CardTitle>
                    <CardTitle>Carburant: {vehicule && vehicule.carburant} </CardTitle>
                    <CardTitle>Prix journalier: {vehicule && vehicule.prixJournalier} Dh </CardTitle>
                        <Col>
                        <Link to={'/vehicules'}>
                            <button className={"btn btn-outline-info"}>Retour à la liste</button>
                        </Link>
                        </Col>
                </CardBody>
            </Card>
        </div>
}

export default Vehicule;