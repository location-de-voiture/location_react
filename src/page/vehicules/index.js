import React, { useEffect } from 'react';
import { Card, CardHeader, CardBody, CardImg, CardTitle, CardSubtitle, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import NavBar from '../navbar/NavBar';
import { getCars, deleteCar } from '../../redux/actions/vehicule-actions';
import { getmodeles } from '../../redux/actions/modele-actions'
import { useDispatch, useSelector } from 'react-redux';

const Vehicules = ({ history }) => {
    const vehicules = useSelector(state => state.vehicule.vehicules);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCars());
        dispatch(getmodeles());
    }, [dispatch])

    const handleDelete = (vehicule) => {
        let signe = window.confirm("Voulez vous supprimer la véhicule?");
        if(!signe) return;
        dispatch(deleteCar(vehicule));
    }

    const showDetails = (vehicule) => {
        history.push({
            pathname: `/vehicules/${vehicule.id}`,
            state: {
                vehicule: vehicule,
            }
        });
        //history.push("/vehicules/" + id);
    }

    const handleUpdate = (vehicule) => {
        history.push({
            pathname: `/vehicules/add/${vehicule.id}`,
            state: {
                vehicule: vehicule,
            }
        });
    }
        return    <div className={"container mt-5"}>
            <NavBar />
            <Card>
                <CardHeader>
                    La listes des vehicules
                    <Link to={"/vehicules/add"}>
                    <button className={"btn btn-primary float-right"}>
                        Ajouter une vehicule
                    </button>
                    </Link>
                    
                </CardHeader>
            </Card>
            <Row> 
                        {
                            vehicules && vehicules.map(vehicule => {
                                return <Col sm="4" key={vehicule.id}>
                                <Card body>
                                <CardImg height="183px" src={vehicule.image && "http://127.0.0.1:8000/media/"+vehicule.image.filePath} alt="Card image cap" />
                                <CardBody>
                                  <CardTitle>{vehicule.modele.marque} {vehicule.modele.modele} </CardTitle>
                                  <CardSubtitle>Prix journalier : {vehicule.prixJournalier}</CardSubtitle>
                                  <CardSubtitle>Carburant : {vehicule.carburant}</CardSubtitle>
                                        <span
                                            onClick={() => showDetails(vehicule)}
                                            className={"btn btn-info mr-2"}>
                                            Details
                                        </span>
                                        <span
                                            onClick={() => handleUpdate(vehicule)}
                                            className={"btn btn-success mr-2"}>
                                            Modifier
                                        </span>
                                        <span
                                            onClick={() => handleDelete(vehicule)}
                                            className={"btn btn-danger mt-2"}>
                                            Supprimer
                                        </span>
                                </CardBody>
                              </Card>
                              </Col>   
                            })
                        }
                        </Row>
        </div>   
}

export default Vehicules;