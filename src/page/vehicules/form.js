import React, { useState, useEffect } from 'react';
import {FormText, Card, CardHeader, CardBody, Input, FormGroup, Label, Col, Row, Dropdown, DropdownToggle, DropdownMenu, DropdownItem} from "reactstrap";
import {Link} from 'react-router-dom';
import NavBar from "../navbar/NavBar";
import { useDispatch, useSelector} from 'react-redux';
import {addCar} from "../../redux/actions/vehicule-actions";

const VehiculeForm =  ({match, location}) => {
    const dispatch = useDispatch();
    const modeles = useSelector(state => state.modele.modeles)
    const [vehicule, setVehicule] = useState(
        {
            numSerie: "",
            matricule: "",
            kilometrage: "",
            couleur: "",
            carburant: "",
            prixJournalier: "",
            modele: {marque: "Selectionner le modele"},
            image: {}
        }
    );
    const [image_view, setImage_view] = useState('')
    const [file, setFile] = useState(null)
    const [dropdownOpen, setDropdownOpen] = useState(false)
    
    const toggle = () => setDropdownOpen(prevState => !prevState);

    useEffect(() => {
        async function fetchData() {
            if(!match.params.id) return;
            const vehicule = location.state.vehicule;
            const src = "http://127.0.0.1:8000/media/" + vehicule.image?.filePath || '';
            setImage_view(src);
            setVehicule(vehicule);
        }
        fetchData();
    }, [])

    const handleChange = (e) => {
        setVehicule({...vehicule, [e.target.name] : e.target.value})
    }
    
    const onImageChange = (e) => {
        setFile(e.target.files[0]);
        let src=URL.createObjectURL(e.target.files[0]);
        setImage_view(src);
    }

    const changeValue = (m) => {
        setVehicule({...vehicule, modele: m});
    }

    const handleMedia = () => {
        const formData = new FormData();
        formData.append('file',file);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            }
        }
        return {formData: formData, config: config};
    }

    const ajouterVehicule = () => {
        const {formData, config} = handleMedia();
        dispatch(addCar(vehicule, formData, config, file));
    }

    const {numSerie, matricule, kilometrage, couleur, carburant, prixJournalier, id} = vehicule;
    const button=<span className={"btn btn-outline-success"} 
    onClick={ajouterVehicule}>
    {id? "Modifier":"Ajouter"}
    </span>
        return <div className={"container mt-5"}>
            <NavBar />
            <Card>
                <CardHeader>Ajouter une vehicule :</CardHeader>
                <CardBody>
                    <form>
                        <FormGroup>
                            <Label>Num Serie : </Label>
                            
                            <Input
                                name={"numSerie"}
                                type={"text"}
                                placeholder={"Num Serie de vehicule"}
                                value={numSerie}
                                onChange={handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Matricule : </Label>
                            <Input
                                name={"matricule"}
                                type={"text"}
                                placeholder={"Matricule de vehicule"}
                                value={matricule}
                                onChange={handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Kilometrage : </Label>
                            <Input
                                name={"kilometrage"}
                                type={"text"}
                                placeholder={"Kilometrage de vehicule"}
                                value={kilometrage}
                                onChange={handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Couleur : </Label>
                            <Input
                                name={"couleur"}
                                type={"text"}
                                placeholder={"Couleur de vehicule"}
                                value={couleur}
                                onChange={handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Carburant : </Label>
                            <Input
                                name={"carburant"}
                                type={"text"}
                                placeholder={"Carburant de vehicule"}
                                value={carburant}
                                onChange={handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label>Prix journalier : </Label>
                            <Input
                                name={"prixJournalier"}
                                type={"text"}
                                placeholder={"Prix journalier de vehicule"}
                                value={prixJournalier}
                                onChange={handleChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="exampleFile">Image</Label>
                            <Input type="file" name="image" id="exampleFile" 
                            onChange={onImageChange} />
                            <FormText color="muted">
                            Choissez l'image du véhicule.
                            </FormText>
                            <img src={image_view} alt="" width="308px" height="183px"></img>
                        </FormGroup>
                        <FormGroup>
                            <Label>Modele : </Label>
                            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                                <DropdownToggle caret>
                                {vehicule.modele && vehicule.modele.marque} {vehicule.modele && vehicule.modele.modele}
                                </DropdownToggle>
                                <DropdownMenu>
                                    {modeles.map(modele => 
                                        <DropdownItem onClick={() => changeValue(modele)} key={modele.id}> {modele.marque} {modele.modele}</DropdownItem>  
                                        )}                                  
                                </DropdownMenu>
                            </Dropdown>
                        </FormGroup>
                        <Row>
                            <Col className={"offset-10"}>
                            {button}
                            </Col>
                        </Row>
                    </form>
                    <Link to={"/vehicules"}>
                        <button className={"btn btn-info"}>Retour à la liste</button>
                    </Link>
                </CardBody>
            </Card>

        </div>;
}

export default VehiculeForm;