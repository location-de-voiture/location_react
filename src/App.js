import React from 'react';
import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import VehiculeForm from "./page/vehicules/form";
import Vehicules from './page/vehicules';
import Vehicule from './page/vehicules/show';
import Modeles from './page/modeles';
import {Provider} from "react-redux";
import store from "./redux/store";

function App() {
  return (
    <Provider store = {store}>
      <BrowserRouter>
              <div className="App">
                  <Switch>
                    <Route path={"/vehicules"} component={Vehicules} exact/>
                    <Route path={"/vehicules/add"} component={VehiculeForm} exact/>
                    <Route path={"/vehicules/:id"} component={Vehicule} exact/>
                    <Route path={"/vehicules/add/:id"} component={VehiculeForm} exact/>
                    <Route path={"/modeles"} component={Modeles} exact/>              
                  </Switch>
              </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
