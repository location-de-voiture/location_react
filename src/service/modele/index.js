import API from "../../config/axios";
import {MODELE_ENDPOINT, MODELE_NO_JSON_ENDPOINT} from "../endpoints";

export const getModeles = () => API.get(MODELE_ENDPOINT)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const postModele = (modele) => API.post(MODELE_ENDPOINT, modele)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const putModele = (modele) => API.put(MODELE_NO_JSON_ENDPOINT + "/" + modele.id, modele)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const deleteModele = (id) => API.delete(MODELE_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const getModele = (id) => API.get(MODELE_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});    