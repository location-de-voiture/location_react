import API from "../../config/axios";
import {VEHICULE_ENDPOINT, VEHICULE_NO_JSON_ENDPOINT} from "../endpoints";

export const getVehicules = () => API.get(VEHICULE_ENDPOINT)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const postVehicule = (vehicule) => API.post(VEHICULE_ENDPOINT, vehicule)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const putVehicule = (vehicule) => API.put(VEHICULE_NO_JSON_ENDPOINT + "/" + vehicule.id, vehicule)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const deleteVehicule = (id) => API.delete(VEHICULE_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const getVehicule = (id) => API.get(VEHICULE_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});    