import API from "../../config/axios";
import {MEDIA_ENDPOINT, MEDIA_NO_JSON_ENDPOINT} from "../endpoints";

export const getMedias = () => API.get(MEDIA_ENDPOINT)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const postMedia = (media, config) => API.post(MEDIA_ENDPOINT, media, config)
    .then(response => response.data)
    .catch(response => {console.log(response)});

export const getMedia = (id) => API.get(MEDIA_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});    

export const deleteMedia = (id) => API.delete(MEDIA_NO_JSON_ENDPOINT + "/" + id)
    .then(response => response.data)
    .catch(response => {console.log(response)});    