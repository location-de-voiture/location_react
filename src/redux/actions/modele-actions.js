import { getModeles } from '../../service/modele'
export const getmodeles = () => {
    return async (dispatch) => {
        const res = await getModeles();
        dispatch({
            type: "GET_MODELES",
            payload: res,
        });
    };
}