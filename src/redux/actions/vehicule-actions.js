import {getVehicules, deleteVehicule, postVehicule, putVehicule} from "../../service/vehicule";
import {deleteMedia} from "../../service/mediaObject";
import {postMedia} from "../../service/mediaObject";
import { history } from "../../index";

export const getCars = () => {
    return async (dispatch) => {
        const res = await getVehicules();
        dispatch({
            type: "GET_VEHICULES",
            payload: res,
        });
    };
}

export const getCar = (vehicule) => {
    return async (dispatch) => {
        dispatch({
            type: "GET_VEHICULE",
            payload: vehicule,
        });
    };
}

export const addCar = (vehicule, formData, config, file) => {
    const {numSerie, matricule, kilometrage, couleur, carburant, prixJournalier, modele, image} = vehicule;
    const data = {
        numSerie,
        matricule,
        kilometrage: parseInt(kilometrage),
        couleur,
        image,
        carburant,
        prixJournalier: parseInt(prixJournalier),
        disponibilite: true,
        createdAt: "2020-09-01T17:50:57.162Z",
        modele: "api/modeles/" + modele.id,
    }
    if(vehicule.id) {
        return (dispatch) => {
            dispatch(editCar(vehicule.id, data, formData, config, file));
        }
    }
    else {
        return async (dispatch) => {
            const response = await postMedia(formData, config);
            data.image = "api/media_objects/" + response.id;
            const res = await postVehicule(data);
            dispatch({
                type: "ADD_VEHICULE",
                payload: res,
            });
            history.push("/vehicules");
        };
    }
}

export const editCar = (id, data, formData, config, file) => {
    return async (dispatch) => {
        const idImage = data.image && data.image.id;
        if(file != null){
            const response = await postMedia(formData, config);
            data.image = "api/media_objects/" + response.id;
        }
        else{
            data.image = "api/media_objects/" + data.image.id;
        }
        data.id = id;
        await putVehicule(data);
        if(file != null) await deleteMedia(idImage);
        debugger;
        dispatch({
            type: "EDIT_VEHICULE",
            payload: data,
        });
        history.push("/vehicules");
    };
}

export const deleteCar = (vehicule) => {
    return async (dispatch) => {
        dispatch({
            type: "DELETE_VEHICULE",
            payload: {
                id: vehicule.id
            },
        });
        await deleteVehicule(vehicule.id);
        await deleteMedia(vehicule.image.id);
    };
}