import {combineReducers} from "redux";
import vehiculeReducer from './vehicule-reducer';
import modeleReducer from './modele-reducer'

const rootReducer = combineReducers({
    vehicule: vehiculeReducer,
    modele: modeleReducer
})

export default rootReducer;