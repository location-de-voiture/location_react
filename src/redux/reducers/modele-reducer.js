const initialState = {
    modeles: [],
}

const modeleReducer = (state = initialState, action) => {
    switch(action.type){
        case "GET_MODELES":
            return {
                ...state,
                modeles: action.payload
            };
        default:
            return state;
    }
}
export default modeleReducer;