const initialState = {
    vehicules: [],
    vehicule: {}
}

const vehiculeReducer = (state = initialState, action) => {
    switch(action.type){
        case "GET_VEHICULES":
            return {
                ...state,
                vehicules: action.payload
            };
        case "GET_VEHICULE":
            return {
                ...state,
                vehicule: action.payload
            };
        case "ADD_VEHICULE":
            return {
                ...state,
                vehicules: [...state.vehicules, action.payload]
            };
        case "EDIT_VEHICULE":
            return {
                ...state,
                vehicules: [...state.vehicules, action.payload]
            };            
        case "DELETE_VEHICULE":
            const updatedList = state.vehicules.filter(vehicule => vehicule.id !== action.payload.id)
            return {
                ...state,
                vehicules: updatedList
            };    
        default:
            return state;
    }
}
export default vehiculeReducer;